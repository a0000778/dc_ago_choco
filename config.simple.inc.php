<?php
date_default_timezone_set('Asia/Taipei');

//Test Version
class Config {
	// DC ID (App, client, ...)
	public const DC_ID = 0;
	private const CONFIG = [
		'Discord\\API' => [
			// DC bot token，呼叫 DC API 驗證用
			'token' => '',
		],
		'Discord\\Request' => [
			// DC public key
			'public_key' => '',
		],
		'event\\message\\Keyword' => [
			// 按序逐一比對
			'keywords' => [
				// 以下所有選項都是可選的
				[
					// 發言內容符合任一 regexp
					'content' => [ '/巧克力/' ],
					// 且發言者 ID 在這之中
					'author' => [ 0 ],
					// 30 分之 1 的機率
					'rate' => [ 1, 30 ],
					// 距離前次觸發最少 600 秒，相同 key 則間距共享
					'cooldown' => [ 'key' => 'call', 'interval' => 600 ],
					// 隨機發送一個訊息
					'message' => [
						'叫我嗎？',
					],
				],
			],
		],
		'job\\PollMsg' => [
			// 監視的頻道ID
			'channels' => [
				'bot' => 0,
			],
		],
	];
	
	public static function auto_config(string $class_name) {
		spl_autoload($class_name);
		if(isset(self::CONFIG[$class_name])){
			$class_name::config(self::CONFIG[$class_name]);
		}
	}
}

set_include_path('./class' . PATH_SEPARATOR . get_include_path());
spl_autoload_extensions('.class.php');
spl_autoload_register('Config::auto_config');

