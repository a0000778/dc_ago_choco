<?php
require './config.inc.php';

function rand_everyday(int $min, int $max, int $seed = 0): int {
	// openssl prime -generate -bits 47
	// 迴避整數相乘後超過 64 bit 的可能性
	// 64 bit - 16 bit (保留 65535 天的空間) - 1 bit (正負號)
	static $p = 112205221869113;
	
	$day = (int)(time() / 86400);
	$range = $max - $min + 1;
	$rand = $p * $day;
	$rand ^= $seed;
	$rand %= $range;
	$rand += $min;
	return $rand;
}

$cron = new Crontab('default');
$cron->interval('頻道掃描', '\\job\\PollMsg::execute', 60);
// 每天早上 6 點
$cron->at('早安', '\\job\\Morning::execute', rand_everyday(0, 30, 235081748549623), 6);

