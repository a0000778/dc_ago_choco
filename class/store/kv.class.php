<?php
namespace store;

class KV {
	public const CONT_PREFIX = "<?php die(); ?>\n";
	public const DATA_DIR = './data/';
	private static array $scopes = [];
	
	private string $scope;
	private bool $view;
	private bool $changed = false;
	private $fp;
	private array $map;

	private static function filepath(string $name): string {
		return self::DATA_DIR . $name . '.data.php';
	}
	private static function load(string $name, bool $view): KV {
		if(preg_match('/^[\p{L}\p{N}\p{Pc}\p{Pd}]+$/', $name) === false){
			throw new \Exception('KV: bad name');
		}
		$kv = new self($name, $view);
		self::$scopes[$name] = $kv;
		return $kv;
	}
	public static function scope(string $name): KV {
		$kv = self::$scopes[$name] ?? self::load($name, false);
		if($kv->view){
			unset($kv);
			unset(self::$scopes[$name]);
			$kv = self::load($name, false);
		}
		return $kv;
	}
	public static function scope_view(string $name): KV {
		return self::$scopes[$name] ?? self::load($name, true);
	}
	public static function flush() {
		foreach(self::$scopes as $kv){
			$kv->flush();
		}
	}
	private function __construct(string $name, bool $view = false) {
		$this->scope = $name;
		$this->view = $view;
		$filepath = self::filepath($name);
		$fp = fopen($filepath, 'c+');
		if($fp === false){
			throw new \Exception('KV: open fail');
		}
		$this->fp = $fp;
		$lock = $view ? LOCK_SH : LOCK_EX;
		if(!flock($this->fp, $lock)){
			throw new \Exception('KV: lock fail');
		}
		$raw = stream_get_contents($this->fp, null, strlen(self::CONT_PREFIX));
		if(strlen($raw) > 0){
			$map = unserialize($raw, [ 'allowed_classes' => false ]);
			if($map === false){
				throw new \Exception('KV: parse fail');
			}
		} else {
			$map = [];
		}
		$this->map = $map;
	}
	function __destruct() {
		if($this->changed){
			if($this->view){
				throw new \Exception('KV: write in view');
			}
			$raw = self::CONT_PREFIX . serialize($this->map);
			fseek($this->fp, 0, SEEK_SET);
			fwrite($this->fp, $raw);
			fflush($this->fp);
		}
		flock($this->fp, LOCK_UN);
		fclose($this->fp);
	}
	public function is_view(): bool {
		return $this->view;
	}
	public function isset(string $key): bool {
		return isset($this->map[$key]);
	}
	public function get(string $key): mixed {
		return $this->map[$key] ?? null;
	}
	public function set(string $key, mixed $value) {
		if(isset($this->map[$key]) && $this->map[$key] === $value){
			return;
		}
		if($this->view){
			throw new \Exception('KV: write in view');
		}
		$this->changed = true;
		$this->map[$key] = $value;
	}
	public function unset(string $key) {
		if(!isset($this->map[$key])){
			return;
		}
		if($this->view){
			throw new \Exception('KV: write in view');
		}
		$this->changed = true;
		unset($this->map[$key]);
	}
}

