<?php
class Event {
	private static array $handlers = [];
	
	public static function load_handlers(string $event) {
		foreach(glob('class/event/' . $event . '/?*.class.php') as $filepath){
			include_once $filepath;
		}
		
		$class_filter = fn($c) => str_starts_with($c, 'event\\' . $event . '\\');
		$event_handlers = array_filter(
			get_declared_classes(),
			$class_filter
		);
		foreach($event_handlers as $h){
			Config::auto_config($h);
		}
		self::$handlers[$event] = $event_handlers;
	}
	public static function emit(string $event, mixed $args) {
		if(!isset(self::$handler[$event])){
			self::load_handlers($event);
		}
		$event_handlers = self::$handlers[$event];
		foreach($event_handlers as $h){
			$h::emit($args);
		}
	}
}

