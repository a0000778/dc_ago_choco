<?php
use \store\KV;

class Crontab {
	public const MAX_DELAY = 600;
	private string $scope;
	private KV $kv;
	
	public function __construct(string $scope) {
		$this->scope = $scope;
		$this->kv = KV::scope('crontab_' . $scope);
	}
	private static function match_prev(int $value, null|int|array $matches, bool $ring): ?int {
		if($matches === null || is_int($matches)){
			return $matches ?? $value;
		}
		if(empty($matches)){
			return $value;
		}
		$matches = array_unique($matches);
		sort($matches, SORT_NUMERIC);
		if(count($matches) === 1){
			return $matches[0];
		}
		if($ring === true){
			$match_min = $matches[array_key_first($matches)];
			if($value < $match_min){
				return $matches[array_key_last($matches)];
			}
		}
		$result = null;
		foreach($matches as $m){
			if($value < $m){
				return $result;
			}
			$result = $m;
		}
		return $result;
	}
	private static function prev_time(array $matches): ?int {
		$now = getdate(time());
		if($matches['wday'] !== null){
			$wday = self::match_prev($now['wday'], $matches['wday'], true);
			if($wday === null){ return null; }
			$mon = $now['mon'];
			$mday = $now['mday'] - ( $now['wday'] - $wday );
		}else{
			$mon = self::match_prev($now['mon'], $matches['mon'], true);
			if($mon === null){ return null; }
			$mday = self::match_prev($now['mday'], $matches['mday'], true);
			if($mday === null){ return null; }
		}
		$hours = self::match_prev($now['hours'], $matches['hours'], true);
		if($hours === null){ return null; }
		$minutes = self::match_prev($now['minutes'], $matches['minutes'], true);
		if($minutes === null){ return null; }
		return mktime($hours, $minutes, 0, $mon, $mday);
	}
	// 指定時間日期或週執行
	public function at(
		string $name,
		callable|string $call,
		null|int|array $minutes = null,
		null|int|array $hours = null,
		null|int|array $wday = null,
		null|int|array $mday = null,
		null|int|array $mon = null,
	) {
		$matches = [
			'minutes' => $minutes,
			'hours' => $hours,
			'wday' => $wday,
			'mday' => $mday,
			'mon' => $mon,
		];
		$prev_time = self::prev_time($matches);
		// 第一次執行時間不符
		if($prev_time === null){ return; }
		
		$key = 'last_' . $name;
		$last_time = $this->kv->get($key) ?? 0;
		// 這輪執行過了
		if($last_time >= $prev_time){ return; }
		
		$now = time();
		$delay = $now - $prev_time;
		// 執行晚了
		if($delay > self::MAX_DELAY){ return; }
		// 未來的事
		if($delay < 0){ return; }
		
		$this->kv->set($key, $now);
		$call();
	}
	// 每隔一定秒數後執行
	public function interval(string $name, callable $call, int $interval) {
		$key = 'last_' . $name;
		$last_time = $this->kv->get($key) ?? 0;
		$now = time();
		if($last_time + $interval > $now){
			return;
		}
		$this->kv->set($key, $now);
		$call();
	}
}

