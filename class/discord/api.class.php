<?php
namespace Discord;

class API {
	public const GATEWAY = 'https://discord.com/api';
	private static $token = '';
	
	public static function config(array $config) {
		self::$token = $config['token'];
	}
	public static function request(
		string $method,
		string $path,
		?array $query = null,
		?array $payload_json = null,
		array $payload = []
	): APIResponse {
		$req = new APIRequest();
		$req->method = $method;
		$req->endpoint = self::GATEWAY . $path;
		$req->query = is_null($query) ? '' : '?' . http_build_query($query);
		$req->headers = [
			'Authorization: Bot ' . self::$token,
		];
		if(!empty($payload_json)){
			$payload['payload_json'] = json_encode($payload_json, JSON_UNESCAPED_UNICODE);
		}
		$req->payload = $payload;
		
		$e = null;
		for($try = 0; $try < 5; $try++){
			try{
				$res = $req->send();
			}catch(\Exception $e){
				continue;
			}
			if($res->status === 429){
				$wait = round((float)$res->headers['x-ratelimit-reset-after'], 0) ?? 3;
				if($wait > 10){
					throw new \Exception('API 速率限制，須等待時間太長 (>10s)');
				}
				sleep($wait);
				continue;
			}else if(in_array($res->status, [400, 401, 403], true)){
				throw new Exception('API 改變、權限設定錯誤或程式錯誤' . $res->status);
			}else if(3 === (int)($res->status / 100)){
				throw new Exception('未定義的 API 回應：' . $res->status);
			}else if(5 === (int)($res->status / 100)){
				sleep(5);
				continue;
			}
			return $res;
		}
		if($e !== null){
			throw $e;
		}
		throw new \Exception('DC API 故障，重試次數超過上限');
	}
}

class APIRequest {
	public string $method;
	public string $endpoint;
	public string $query;
	public array $headers = [];
	public array $payload = [];
	
	public function send(): APIResponse {
		$curl = curl_init($this->endpoint . $this->query);
		curl_setopt_array($curl, [
			CURLOPT_CUSTOMREQUEST => $this->method,
			CURLOPT_HTTPHEADER => $this->headers,
			CURLOPT_SAFE_UPLOAD => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,
			CURLOPT_TIMEOUT_MS => 1000,
		]) || throw new \Exception('curl_setopt fail');
		if(!empty($this->payload)){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $this->payload);
		}
		$raw_result = curl_exec($curl);
		if($raw_result === false){
			throw new \Exception(
				curl_error($curl),
				curl_errno($curl)
			);
		}
		return APIResponse::from_raw($raw_result);
	}
}

class APIResponse {
	public int $status;
	public array $headers;
	public string $body;
	
	public function __construct(int $status, array $headers, string $body) {
		$this->status = $status;
		$this->headers = $headers;
		$this->body = $body;
	}
	private static function parse_header(string $header_str): array {
		preg_match_all(
			'/([A-Za-z\d]*(?:-[A-Za-z\d]+)*): ([^\r\n]+)(?:\r\n|\r|\n|$)/u',
			$header_str,
			$m,
			PREG_SET_ORDER
		);
		return array_column($m, 2, 1);
	}
	public static function from_raw(string $raw): APIResponse {
		list($raw_header, $body) = explode("\r\n\r\n", $raw, 2);
		list($http, $raw_header) = explode("\r\n", $raw_header, 2);
		list($http, $status) = explode(' ', $http, 2);
		$headers = self::parse_header($raw_header);
		$res = new self((int)$status, $headers, $body);
		return $res;
	}
	public function json(): mixed {
		switch($this->status){
			case 200:
				return json_decode($this->body, true);
			case 204:
			case 404:
				return null;
			default:
				throw new \Exception('API: 未知結果: ' . $this->status);
		}
	}
}

