<?php
namespace Discord;

use \impl\iter\BufList;

class Channel {
	public int $channel_id;
	public string $name;
	public int $last_message_id;
	
	public static function get(int $channel_id): ?self {
		$result = API::request(
			'GET',
			'/channels/' . $channel_id,
		)->json();
		if($result === null){
			return null;
		}
		$ch = new Channel();
		$ch->channel_id = $result['id'];
		$ch->name = $result['name'];
		$ch->last_message_id = $result['last_message_id'];
		return $ch;
	}
	public static function messages(int $channel_id, int $after): iterable {
		return new ChannelMessages($channel_id, $after);
	}
}

// 循序訊息跌代器，查看最後的訊息要另外實現反序
class ChannelMessages extends BufList {
	private int $channel_id;
	private ?string $last_key = null;

	public function __construct(int $channel_id, int $init_key){
		parent::__construct($init_key);
		$this->channel_id = $channel_id;
	}
	protected function fill_buf(mixed $key): array {
		if($key === null){
			return null;
		}
		$result = API::request(
			'GET',
			'/channels/' . $this->channel_id . '/messages',
			[ 'after' => $key ]
		)->json();
		$result = array_reverse($result);
		$result = array_map(__NAMESPACE__ . '\\Message::from_array', $result);
		$last_key = array_key_last($result);
		$this->last_key = $last_key !== null ? $result[$last_key]->id : null;
		return $result;
	}
	protected function next_buf_key(): mixed {
		return $this->last_key;
	}
	public function key(): mixed {
		return $this->current()->id;
	}
}

