<?php
namespace Discord;

class Message {
	public int $id;
	public int $channel_id;
	public string $content;
	public int $timestamp;
	public User $author;
	public array $mentions = [];
	public array $mention_channels = [];
	public array $attachments = [];

	private static function parse_time(string $dc_time): int {
		static $time_zone = new \DateTimeZone('Asia/Taipei');
		$ts = \DateTime::createFromFormat(
			'Y-m-d\\TH:i:s.uP',
			$dc_time,
			$time_zone
		);
		if($ts === false){
			// DC有病，新訊息時間可能不帶毫秒
			$ts = \DateTime::createFromFormat(
				'Y-m-d\\TH:i:sP',
				$dc_time,
				$time_zone
			);
		}
		if($ts === false){
			throw new \Exception('Discord timestamp parse fail');
		}
		return $ts->getTimestamp();
	}
	public static function from_array(array $src): self {
		$msg = new Message();
		$msg->id = $src['id'];
		$msg->channel_id = $src['channel_id'];
		$msg->content = $src['content'];
		$msg->timestamp = self::parse_time($src['timestamp']);
		$msg->author = new User($src['author']['id'], $src['author']['username']);
		$msg->mentions = $src['mentions'];
		$msg->mention_channels = $src['mention_channels'] ?? [];
		$msg->attachments = $src['attachments'];
		return $msg;
	}
	public static function send(
		int $channel_id,
		string $content,
		array $embeds = [],
	): ?Message {
		$result = API::request(
			'POST',
			'/channels/' . $channel_id . '/messages',
			null,
			[
				'content' => $content,
			]
		)->json();
		if($result === null){
			die('發送失敗');
		}
		return self::from_array($result);
	}
}

