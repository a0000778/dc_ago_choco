<?php
namespace event\message;

use \Discord\Message;
use \store\KV;

class Keyword {
	public const MAX_DELAY = 600;
	public static array $keywords;
	public static function config(array $config) {
		self::$keywords = $config['keywords'];
	}
	public static function emit(Message $msg) {
		$now = time();
		if($now - $msg->timestamp > self::MAX_DELAY){ return; }
		foreach(self::$keywords as $match){
			if(isset($match['content'])){
				$matched = false;
				foreach($match['content'] as $reg){
					if(preg_match($reg, $msg->content) !== 0){
						$matched = true;
						break;
					}
				}
				if($matched === false){ continue; }
			}
			if(isset($match['author'])){
				if(!in_array($msg->author->id, $match['author'])){
					continue;
				}
			}
			if(isset($match['rate'])){
				$rand = rand(1, $match['rate'][1]);
				if($rand > $match['rate'][0]){
					continue;
				}
			}
			if(isset($match['cooldown'])){
				$now = time();
				$kv = KV::scope('ev_msg_keyword_cooldown');
				$prev_time = $kv->get($match['cooldown']['key']) ?? 0;
				if($prev_time + $match['cooldown']['interval'] > $now){
					continue;
				}
				$kv->set($match['cooldown']['key'], $now);
			}
			if(isset($match['message'])){
				Message::send(
					$msg->channel_id,
					$match['message'][array_rand($match['message'])]
				);
			}
			break;
		}
	}
}

