<?php
namespace impl\iter;

abstract class BufList implements \Iterator {
	private mixed $init_key;
	private mixed $buf_key = null;
	private array $buf = [];
	private int $pos = 0;
	
	abstract protected function fill_buf(mixed $key): array;
	abstract protected function next_buf_key(): mixed;
	abstract public function key(): mixed;
	protected function __construct(mixed $init_key) {
		$this->init_key = $init_key;
	}
	public function current(): mixed {
		return $this->buf[$this->pos];
	}
	public function next(): void {
		$this->pos++;
		if(isset($this->buf[$this->pos])){
			return;
		}
		$this->pos = 0;
		$this->buf_key = $this->next_buf_key();
		$this->buf = $this->fill_buf($this->buf_key);
	}
	public function rewind(): void {
		$this->pos = 0;
		if($this->buf_key === $this->init_key){
			return;
		}
		$this->buf_key = $this->init_key;
		$this->buf = $this->fill_buf($this->buf_key);
	}
	public function valid(): bool {
		return isset($this->buf[$this->pos]);
	}
}

