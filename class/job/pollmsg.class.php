<?php
namespace job;

use \Discord\Channel;
use \Event;
use \store\KV;

class PollMsg {
	public static array $channels = [];
	private int $channel_id;
	
	public static function config(array $config) {
		self::$channels = $config['channels'] ?? [];
	}
	public static function execute() {
		foreach(self::$channels as $channel_id){
			$ch = new self($channel_id);
			$ch->poll();
		}
	}
	public function __construct(int $channel_id) {
		$this->channel_id = $channel_id;
	}
	public function poll() {
		$kv = KV::scope('pollmsg-'.$this->channel_id);
		$last_msg_id = $kv->get('last_msg_id');
		if($last_msg_id === null){
			$last_msg_id = Channel::get($this->channel_id)->last_message_id ?? null;
			if($last_msg_id !== null){
				$kv->set('last_msg_id', $last_msg_id);
			}
			return;
		}
		foreach(Channel::messages($this->channel_id, $last_msg_id) as $msg){
			if($msg->author->id === \Config::DC_ID){ continue; }
			Event::emit('message', $msg);
			$kv->set('last_msg_id', $msg->id);
		}
	}
}

