<?php
namespace job;

use \Discord\Message;

class Morning {
	private static int $channel_id;
	
	public static function config(array $config) {
		self::$channel_id = $config['channel_id'];
	}
	public static function execute() {
		Message::send(self::$channel_id, '靈靈早安，大家早安');
	}
}

